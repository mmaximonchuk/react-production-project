import about from './about.json';
import main from './main.json';
import notFound from './notFound.json';
import translation from './translation.json';

const allTranslations = {
  ...about,
  ...main,
  ...notFound,
  ...translation,
};

export default allTranslations;
