import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Button } from 'shared';

// Компонент для тестирования
export const BugButton = () => {
  const [error, setError] = useState(false);
  const { t } = useTranslation();

  useEffect(() => {
    if (!error) return;

    throw new Error('You got into trouble');
  }, [error]);

  return <Button onClick={() => setError(true)}>{t('throwError')}</Button>;
};
