import { FC, useMemo, useState } from 'react';
import {
  LOCAL_STORAGE_THEME_KEY,
  ThemeContext,
  Themes,
} from 'app/providers/ThemeProvider/lib/ThemeContext';

interface InitialThemeProps {
  initialTheme?: Themes;
}

const defaultTheme =
  (localStorage.getItem(LOCAL_STORAGE_THEME_KEY) as Themes) || Themes.LIGHT;

export const ThemeContextProvider: FC<InitialThemeProps> = ({
  children,
  initialTheme,
}) => {
  const [theme, setTheme] = useState<Themes>(initialTheme ?? defaultTheme);

  const defaultProps = useMemo(() => ({ theme, setTheme }), [theme]);

  document.body.className = theme;

  return (
    <ThemeContext.Provider value={defaultProps}>
      {children}
    </ThemeContext.Provider>
  );
};
