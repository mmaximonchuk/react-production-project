import { Suspense, useState } from 'react';

import { useTheme } from 'app/providers/ThemeProvider';

import { NavBar, PageLoader, SideBar } from 'widgets';

import { classNames } from 'shared';
import { AppRouter } from './providers/router';

export default function App() {
  const { theme } = useTheme();

  return (
    <div className={classNames('app', {}, [])}>
      <Suspense fallback={<PageLoader />}>
        <NavBar />
        <div className='content-page'>
          <SideBar />
          <AppRouter />
        </div>
      </Suspense>
    </div>
  );
}
