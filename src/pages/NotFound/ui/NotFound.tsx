import { useTranslation } from 'react-i18next';

import { classNames } from 'shared';

import css from './NotFound.module.scss';

interface NotFoundProps {
  className?: string;
}

const NotFound = ({ className }: NotFoundProps) => {
  const { t } = useTranslation();
  return (
    <div className={classNames(css.NotFound, {}, [className])}>
      {t('notFound.title')}
    </div>
  );
};

export default NotFound;
