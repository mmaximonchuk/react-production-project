import { useTranslation } from 'react-i18next';

type Props = {};

function AboutPage(props: Props) {
  const { t } = useTranslation();
  return <div>{t('aboutPage.title')}</div>;
}

export default AboutPage;
