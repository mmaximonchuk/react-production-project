import { BugButton } from 'app/providers/ErrorBoundary';
import { Counter } from 'entities/Counter';
import { useTranslation } from 'react-i18next';
// import { Text } from 'shared';

type Props = {};

function MainPage(props: Props) {
  const { t } = useTranslation();
  return (
    <div>
      {/* <Text as='p'>Awdaw</Text> */}
      <BugButton />
      {t('mainPage.title')}
      <Counter />
    </div>
  );
}

export default MainPage;
