import { fireEvent, screen, waitFor } from '@testing-library/react';

import { componentRender } from 'shared/lib/tests/componentRender/componentRender';
import { Counter } from './Counter';

describe('Counter', () => {
  test('render Sidebar', async () => {
    componentRender(() => <Counter />, {
      initialState: {
        counter: { value: 10 },
      },
    });

    await waitFor(() => {
      const counterValueTitle = screen.getByTestId('value-title');
      const counter = screen.getByTestId('counter');

      expect(counter).toBeInTheDocument();
      expect(counterValueTitle).toHaveTextContent('10');
    });
  });

  test('collapse increment button click', async () => {
    componentRender(() => <Counter />, {
      initialState: {
        counter: { value: 10 },
      },
    });

    await waitFor(() => {
      const counterValueTitle = screen.getByTestId('value-title');
      const incrementBtn = screen.getByTestId('increment-btn');

      // toggleBtn.click();
      fireEvent.click(incrementBtn);

      expect(counterValueTitle).toHaveTextContent('11');
    });
  });

  test('collapse decrement button click', async () => {
    componentRender(() => <Counter />, {
      initialState: {
        counter: { value: 10 },
      },
    });

    await waitFor(() => {
      const counterValueTitle = screen.getByTestId('value-title');
      const decrementBtn = screen.getByTestId('decrement-btn');

      // toggleBtn.click();
      fireEvent.click(decrementBtn);

      expect(counterValueTitle).toHaveTextContent('9');
    });
  });
});
