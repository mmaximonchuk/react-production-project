import { Loader, classNames } from 'shared';

import css from './PageLoader.module.scss';

interface PageLoaderProps {
  className?: string;
}

export const PageLoader = ({ className }: PageLoaderProps) => (
  <div className={classNames(css.PageLoader, {}, [className])}>
    <Loader />
  </div>
);
