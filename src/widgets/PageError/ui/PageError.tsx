import { useTranslation } from 'react-i18next';

import { Button, classNames } from 'shared';

import css from './PageError.module.scss';

interface PageErrorProps {
  className?: string;
}

export const PageError = ({ className }: PageErrorProps) => {
  const { t, i18n } = useTranslation();
  console.log('i18n: ', i18n);

  const reloadPage = () => {
    window.location.reload();
  };

  return (
    <div className={classNames(css.PageError, {}, [className])}>
      <h1>{t('unexpectedError')}</h1>
      <Button onClick={reloadPage}>{t('reloadPage')}</Button>
    </div>
  );
};
