import type { Meta, StoryObj } from '@storybook/react';

import { ThemeDecorator } from 'shared/config/storybook/ThemeDecorator';
import { I18nDecorator } from 'shared/config/storybook/I18nDecorator';

import { Themes } from 'app/providers/ThemeProvider';

import { PageError } from './PageError';

const meta = {
  title: 'widget/PageError',
  component: PageError,
  decorators: [I18nDecorator],
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/react/configure/story-layout
  },
  argTypes: {},
} satisfies Meta<typeof PageError>;

type Story = StoryObj<typeof meta>;

export const Light: Story = {};

export const Dark: Story = {
  args: {},
};

Dark.decorators = [ThemeDecorator(Themes.DARK)];

export default meta;
