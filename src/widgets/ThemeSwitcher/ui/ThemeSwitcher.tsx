import { Themes, useTheme } from 'app/providers/ThemeProvider';

import { Button, ButtonVariants, classNames } from 'shared';

import IconLight from 'widgets/assets/icons/theme-light.svg';
import IconDark from 'widgets/assets/icons/theme-dark.svg';

import css from './ThemeSwitcher.module.scss';

interface ThemeSwitcherProps {
  className?: string;
}

export function ThemeSwitcher({ className }: ThemeSwitcherProps) {
  const { theme, toggleTheme } = useTheme();

  return (
    <Button
      className={classNames(css.ThemeSwitcher, {}, [className])}
      onClick={toggleTheme}
      variant={ButtonVariants.CLEAR}
      aria-label={theme === Themes.DARK ? 'Dark Theme' : 'Light Theme'}
    >
      {theme === Themes.DARK ? <IconDark /> : <IconLight />}
    </Button>
  );
}
