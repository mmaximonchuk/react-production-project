import { useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ThemeSwitcher } from 'widgets';

import {
  AppLink,
  AppLinkTheme,
  Button,
  ButtonVariants,
  LangSwitcher,
  // MySuperButton,
  RouterPath,
  Text,
  classNames,
} from 'shared';

import IconHome from 'shared/assets/icons/house.svg';
import IconAbout from 'shared/assets/icons/clarity-list.svg';

import { ButtonSize } from 'shared/ui/Button/Button';
import css from './SideBar.module.scss';

interface SideBarProps {
  className?: string;
}

function SideBar({ className }: SideBarProps) {
  const { t } = useTranslation();
  const [collapsed, setCollapsed] = useState(false);

  const onToggle = () => setCollapsed((prev) => !prev);

  return (
    <div
      className={classNames(css.SideBar, { [css.collapsed]: collapsed }, [
        className,
      ])}
      data-testid='sidebar'
    >
      <div className={css.links}>
        <div className={css.item}>
          <AppLink
            theme={AppLinkTheme.SECONDARY}
            className={css.link}
            to={RouterPath.main}
          >
            <IconHome className={css.icon} />
            <Text
              as='span'
              className={css.linkText}
            >
              {t('general')}
            </Text>
          </AppLink>
        </div>
        <div className={css.item}>
          <AppLink
            theme={AppLinkTheme.SECONDARY}
            className={css.link}
            to={RouterPath.about}
          >
            <IconAbout className={css.icon} />
            <Text
              as='span'
              className={css.linkText}
            >
              {t('aboutUs')}
            </Text>
          </AppLink>
        </div>
      </div>

      <Button
        data-testid='sidebar-toggle-btn'
        onClick={onToggle}
        className={css.collapseBtn}
        variant={ButtonVariants.BACKGROUND_INVERTED}
        size={ButtonSize.L}
        square
      >
        {collapsed ? '>' : '<'}
      </Button>
      <div className={css.switchers}>
        <ThemeSwitcher />
        <LangSwitcher shorten={collapsed} />
      </div>
    </div>
  );
}
export default SideBar;
