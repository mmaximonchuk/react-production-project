import { fireEvent, screen, waitFor } from '@testing-library/react';

import { SideBar } from 'widgets/SideBar';
import { componentRender } from 'shared/lib/tests/componentRender/componentRender';

describe('Sidebar', () => {
  test('render Sidebar', async () => {
    componentRender(() => <SideBar className='my-sidebar' />);
    await waitFor(() => {
      expect(screen.getByTestId('sidebar')).toBeInTheDocument();
    });
  });

  test('collapse Sidebar button click', async () => {
    componentRender(() => <SideBar className='my-sidebar' />);

    await waitFor(() => {
      const toggleBtn = screen.getByTestId('sidebar-toggle-btn');

      // toggleBtn.click();
      fireEvent.click(toggleBtn);

      expect(screen.getByTestId('sidebar')).toHaveClass('collapsed');
    });
  });
});
