import { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  AppLink,
  AppLinkTheme,
  Button,
  ButtonVariants,
  classNames,
  Modal,
} from 'shared';

// import imgUserAvatar from "widgets/assets/icons/user-avatar.png";

import css from './NavBar.module.scss';

interface NavBarProps {
  className?: string;
}

export function NavBar({ className }: NavBarProps) {
  const { t } = useTranslation();
  const [isOpenAuthModal, setIsOpenAuthModal] = useState(false);

  const onToggleModal = useCallback(() => {
    setIsOpenAuthModal((prev) => !prev);
  }, []);

  return (
    <nav className={classNames(css.NavBar, {}, [className])}>
      <div className={css.links}>
        <AppLink
          theme={AppLinkTheme.SECONDARY}
          to='/'
        >
          {t('general')}
        </AppLink>
        <Button
          className={css.authBtn}
          variant={ButtonVariants.CLEAR_INVERTED}
          onClick={onToggleModal}
        >
          {t('signIn')}
        </Button>
      </div>{' '}
      <Modal
        isOpen={isOpenAuthModal}
        onClose={onToggleModal}
      >
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur
        reiciendis placeat dolor similique inventore alias modi delectus nulla
        libero in.
      </Modal>
    </nav>
  );
}
