import { BrowserRouter } from 'react-router-dom';
import { render } from 'react-dom';

import App from 'app/App';

import 'app/styles/index.scss';
import 'shared/config/i18n/i18n';

import { ThemeContextProvider } from 'app/providers/ThemeProvider';

import { ErrorBoundary } from 'app/providers/ErrorBoundary';
import { StoreProvider } from 'app/providers/StoreProvider';

render(
  <StoreProvider>
    <BrowserRouter>
      <ErrorBoundary>
        <ThemeContextProvider>
          <App />
        </ThemeContextProvider>
      </ErrorBoundary>
    </BrowserRouter>
  </StoreProvider>,
  document.getElementById('root')
);
