export { classNames } from './lib/classNames/classNames';
export {
  routeConfig,
  RouterPath,
  AppRoutes,
} from './config/routeConfig/routeConfig';

export { AppLink, AppLinkTheme } from './ui/AppLink/AppLink';
export { Button, ButtonVariants } from './ui/Button/Button';
export { MySuperButton } from './ui/MySuperButton/MySuperButton';
export { LangSwitcher } from './ui/LangSwitcher/LangSwitcher';
export { Text } from './ui/Text/Text';
export { Loader } from './ui/Loader/Loader';
export { Modal } from './ui/Modal/Modal';
export { Portal } from './ui/Portal/Portal';

export * from './config/i18n/i18n';
export {
  enTransltaions,
  ruTransltaions,
  uaTransltaions,
} from './config/locales';
