import { ComponentProps, ElementType, ReactNode } from 'react';
import { classNames } from 'shared';

import css from './Text.module.scss';

type TextOwnProps<E extends ElementType = ElementType> = {
  className?: string;
  children: ReactNode;
  as?: E;
};

type TextProps<E extends ElementType> = TextOwnProps<E> &
  Omit<ComponentProps<E>, keyof TextOwnProps>;

const defaultTag = 'p';

export function Text<E extends ElementType = typeof defaultTag>({
  className,
  children,
  as,
  ...restProps
}: TextProps<E>) {
  const classes = classNames(css.Text, {}, [className]);
  const TagName = as ?? defaultTag;

  return (
    <TagName
      className={classes}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    >
      {children}
    </TagName>
  );
}
