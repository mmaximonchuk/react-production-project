import { useCallback, useEffect, useRef, useState } from 'react';
import { classNames } from 'shared/lib/classNames/classNames';
import { useTheme } from 'app/providers/ThemeProvider';
import css from './Modal.module.scss';
import { Portal } from '../Portal/Portal';

type Props = {
  className?: string;
  children?: React.ReactNode;
  isOpen?: boolean;
  onClose?: () => void;
};

const ANIMATION_DELAY = 300;

const Modal = ({ className, children, onClose, isOpen }: Props) => {
  const [isClosing, setIsClosing] = useState(false);
  const timerRef = useRef<ReturnType<typeof setTimeout>>();

  const onCloseHandler = useCallback(
    (e?: React.MouseEvent<HTMLDivElement>) => {
      if (onClose && e?.target === e?.currentTarget) {
        setIsClosing(true);
        timerRef.current = setTimeout(() => {
          setIsClosing(false);
          onClose();
        }, ANIMATION_DELAY);
      }
    },
    [onClose]
  );

  useEffect(() => {
    const handleKeyDown = (e: KeyboardEvent) => {
      if (e.code === 'Escape') {
        onCloseHandler();
      }
    };

    if (isOpen) window.addEventListener('keydown', handleKeyDown);

    return () => {
      clearTimeout(timerRef.current);
      window.removeEventListener('keydown', handleKeyDown);
    };
  }, [isOpen, onCloseHandler]);

  const mods: Record<string, boolean> = {
    [css.opened]: isOpen,
    [css.isClosing]: isClosing,
  };

  return (
    <Portal element={document.querySelector('#modal-root') as HTMLElement}>
      <div className={classNames(css.Modal, mods, [className])}>
        <div
          onClick={onCloseHandler}
          className={css.overlay}
        >
          <div className={css.content}>{children}</div>
        </div>
      </div>
    </Portal>
  );
};

export { Modal };
