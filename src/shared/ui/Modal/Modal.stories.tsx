import type { Meta, StoryObj } from '@storybook/react';

import { ThemeDecorator } from 'shared/config/storybook/ThemeDecorator';

import { Themes } from 'app/providers/ThemeProvider';

import { Modal } from './Modal';

const meta = {
  title: 'shared/Modal',
  component: Modal,
  parameters: {},
  args: {
    isOpen: true,
  },
  argTypes: {},
} satisfies Meta<typeof Modal>;

type Story = StoryObj<typeof meta>;

export const Light: Story = {
  args: {
    children: (
      <>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur
        reiciendis placeat dolor similique inventore alias modi delectus nulla
        libero in.
      </>
    ),
  },
};

export const Dark: Story = {
  args: {
    children: (
      <>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Consectetur
        reiciendis placeat dolor similique inventore alias modi delectus nulla
        libero in.
      </>
    ),
  },
};

Dark.decorators = [ThemeDecorator(Themes.DARK)];

export default meta;
