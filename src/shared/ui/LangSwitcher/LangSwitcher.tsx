import { useTranslation } from 'react-i18next';

import { Button, ButtonVariants, classNames } from 'shared';

interface LangSwitcherProps {
  className?: string;
  shorten?: boolean;
}

const changlLangs: Record<string, string> = {
  en: 'ru',
  ru: 'uk',
  uk: 'en',
};

export function LangSwitcher({ className, shorten }: LangSwitcherProps) {
  const { t, i18n } = useTranslation();

  const toggle = async () => {
    await i18n.changeLanguage(changlLangs[i18n.language]);
  };

  return (
    <div className={classNames('', {}, [className])}>
      <Button
        variant={ButtonVariants.CLEAR}
        onClick={toggle}
      >
        {t(shorten ? 'lang-shorten' : 'lang')}
      </Button>
    </div>
  );
}
