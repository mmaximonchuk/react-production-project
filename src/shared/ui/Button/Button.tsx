import { ButtonHTMLAttributes, FC, PropsWithChildren } from 'react';

import { classNames } from 'shared';

import css from './Button.module.scss';

export enum ButtonVariants {
  CLEAR = 'clear',
  CLEAR_INVERTED = "clearInverted",
  OUTLINE = 'outline',
  BACKGROUND = 'background',
  BACKGROUND_INVERTED = 'backgroundInverted',
}

export enum ButtonSize {
  M = 'size_m',
  L = 'size_l',
  XL = 'size_xl',
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  className?: string;
  variant?: ButtonVariants;
  square?: boolean;
  size?: ButtonSize;
}

export const Button: FC<ButtonProps> = ({
  className,
  children,
  variant,
  square,
  size = ButtonSize.M,
  ...restProps
}: ButtonProps) => {
  const mods: Record<string, boolean> = {
    [css.square]: square,
  };

  return (
    <button
      type='button'
      className={classNames(css.Button, mods, [
        className,
        css[variant],
        css[size],
      ])}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    >
      {children}
    </button>
  );
};
