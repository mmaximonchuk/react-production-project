import { fireEvent, render, screen } from '@testing-library/react';
import { Button, ButtonVariants } from 'shared';

describe('Render Button', () => {
  test('render in the document with text', () => {
    render(<Button>Test</Button>);

    expect(screen.getByText('Test')).toBeInTheDocument();
  });

  test('render in the document with clear variant', () => {
    render(<Button variant={ButtonVariants.CLEAR}>Test</Button>);

    expect(screen.getByText('Test')).toHaveClass('clear');
  });

  test('check button onClick handler', () => {
    const onClickHandler = jest.fn();

    render(
      <Button
        data-testid='test-fn-button'
        onClick={onClickHandler}
        variant={ButtonVariants.CLEAR}
      >
        Test
      </Button>
    );

    const button = screen.getByTestId('test-fn-button');

    fireEvent.click(button);

    expect(onClickHandler).toHaveBeenCalledTimes(1);
  });
});
