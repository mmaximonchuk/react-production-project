import type { Meta, StoryObj } from '@storybook/react';

import { ThemeDecorator } from 'shared/config/storybook/ThemeDecorator';
import { Themes } from 'app/providers/ThemeProvider';
import { Button, ButtonSize, ButtonVariants } from './Button';

const meta = {
  title: 'shared/Button',
  component: Button,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/react/configure/story-layout
    // layout: 'centered',
  },
  argTypes: {},
  args: {
    children: 'Button Example',
  },
} satisfies Meta<typeof Button>;

type Story = StoryObj<typeof meta>;

export const Primary: Story = {
  args: {
    // children: 'Button',
  },
};

Primary.decorators = [ThemeDecorator()];

export const Clear: Story = {
  args: {
    variant: ButtonVariants.CLEAR,
    // children: 'Button',
  },
};

Clear.decorators = [ThemeDecorator()];

export const ClearInverted: Story = {
  args: {
    variant: ButtonVariants.CLEAR_INVERTED,
  },
};

ClearInverted.decorators = [ThemeDecorator()];

export const Outlined: Story = {
  args: {
    variant: ButtonVariants.OUTLINE,
  },
};

Outlined.decorators = [ThemeDecorator()];

export const OutlinedSizeM: Story = {
  args: {
    variant: ButtonVariants.OUTLINE,
    size: ButtonSize.M,
  },
};

OutlinedSizeM.decorators = [ThemeDecorator()];

export const OutlinedSizeL: Story = {
  args: {
    variant: ButtonVariants.OUTLINE,
    size: ButtonSize.L,
  },
};

OutlinedSizeL.decorators = [ThemeDecorator()];

export const OutlinedSizeXL: Story = {
  args: {
    variant: ButtonVariants.OUTLINE,
    size: ButtonSize.XL,
  },
};

OutlinedSizeXL.decorators = [ThemeDecorator()];

export const OutlinedDark: Story = {
  args: {
    variant: ButtonVariants.OUTLINE,
    // children: 'Button',
  },
};

OutlinedDark.decorators = [ThemeDecorator(Themes.DARK)];


export const Background: Story = {
  args: {
    variant: ButtonVariants.BACKGROUND,
    // children: 'Button',
  },
};

Background.decorators = [ThemeDecorator()];

export const InvertedBackground: Story = {
  args: {
    variant: ButtonVariants.BACKGROUND_INVERTED,
    // children: 'Button',
  },
};

InvertedBackground.decorators = [ThemeDecorator()];

export const Square: Story = {
  args: {
    variant: ButtonVariants.BACKGROUND_INVERTED,
    square: true,
    children: '>',
  },
};

Square.decorators = [ThemeDecorator()];

export const SquareSizeM: Story = {
  args: {
    variant: ButtonVariants.BACKGROUND_INVERTED,
    square: true,
    size: ButtonSize.M,
    children: '>',
  },
};

SquareSizeM.decorators = [ThemeDecorator()];

export const SquareSizeL: Story = {
  args: {
    variant: ButtonVariants.BACKGROUND_INVERTED,
    square: true,
    size: ButtonSize.L,
    children: '>',
  },
};

SquareSizeL.decorators = [ThemeDecorator()];

export const SquareSizeXL: Story = {
  args: {
    variant: ButtonVariants.BACKGROUND_INVERTED,
    size: ButtonSize.XL,
    square: true,
    children: '>',
  },
};

SquareSizeXL.decorators = [ThemeDecorator()];


export default meta;
