import { classNames } from 'shared';

import css from './Loader.module.scss';

interface LoaderProps {
  className?: string;
}

export const Loader = ({ className }: LoaderProps) => (
  <div className={classNames(css.Loader, {}, [className])}>
    <div className={css['lds-hourglass']} />
  </div>
);
