import { FC } from 'react';
import { Link, LinkProps } from 'react-router-dom';

import { classNames } from 'shared';
import css from './AppLink.module.scss';

export enum AppLinkTheme {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
}

interface AppLinkProps extends LinkProps {
  className?: string;
  theme?: AppLinkTheme;
}
export const AppLink: FC<AppLinkProps> = ({
  className,
  children,
  to = '',
  theme = AppLinkTheme.PRIMARY,
  ...restProps
}) => (
  <Link
    to={to}
    className={classNames(css.AppLink, {}, [className, css[theme]])}
    {...restProps}
  >
    {children}
  </Link>
);
