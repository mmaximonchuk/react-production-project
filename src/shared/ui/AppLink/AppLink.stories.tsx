import type { Meta, StoryObj } from '@storybook/react';

import { ThemeDecorator } from 'shared/config/storybook/ThemeDecorator';
import { Themes } from 'app/providers/ThemeProvider';
import { AppLink, AppLinkTheme } from './AppLink';

const meta = {
  title: 'shared/AppLink',
  component: AppLink,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/react/configure/story-layout
    // layout: 'centered',
  },
  argTypes: {},
  args: {
    children: 'App link',
    to: '/',
  },
} satisfies Meta<typeof AppLink>;

type Story = StoryObj<typeof meta>;

export const Primary: Story = {};
export const DarkPrimary: Story = {};
DarkPrimary.decorators = [ThemeDecorator(Themes.DARK)];

export const Secondary: Story = {
  args: {
    theme: AppLinkTheme.SECONDARY,
  },
};

export const DarkSecondary: Story = {};
DarkSecondary.decorators = [ThemeDecorator(Themes.DARK)];

export default meta;
