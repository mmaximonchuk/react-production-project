import { ComponentProps, ElementType } from 'react';

import { classNames } from 'shared';

import css from './MySuperButton.module.scss';

type MySuperButtonOwnProps<E extends ElementType = ElementType> = {
  children: string;
  className?: string;
  primary?: boolean;
  secondary?: boolean;
  as?: E;
};

type MySuperButtonProps<E extends ElementType> = MySuperButtonOwnProps<E> &
  Omit<ComponentProps<E>, keyof MySuperButtonOwnProps>;

const defaultElement = 'button';

export function MySuperButton<E extends ElementType = typeof defaultElement>({
  primary,
  secondary,
  className,
  children,
  as,
  ...restProps
}: MySuperButtonProps<E>) {
  const classes = classNames(null, { primary, secondary }, [className]);
  const TagName = as ?? defaultElement;

  return (
    <TagName
      className={classes}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...restProps}
    >
      {children}
    </TagName>
  );
}
