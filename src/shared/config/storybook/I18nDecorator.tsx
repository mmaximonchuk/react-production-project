import { Suspense, useEffect } from 'react';
import { I18nextProvider } from 'react-i18next';
import { Preview, StoryFn } from '@storybook/react';

import { Loader } from 'shared/ui/Loader/Loader';

import i18n from '../i18n/i18n';

export const I18nDecorator = (Story: StoryFn, { globals }: Preview) => {
  const { locale } = globals;

  useEffect(() => {
    i18n.changeLanguage(locale);
  }, [locale]);

  return (
    <Suspense fallback={<Loader />}>
      <I18nextProvider i18n={i18n}>
        <Story />
      </I18nextProvider>
    </Suspense>
  );
};
