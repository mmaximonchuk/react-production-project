import { Preview, StoryFn } from '@storybook/react';
import { ThemeContextProvider, Themes } from 'app/providers/ThemeProvider';
import 'app/styles/index.scss';
import { Suspense } from 'react';
import { Loader } from 'shared/ui/Loader/Loader';

export const ThemeDecorator = (initialTheme?: Themes) => {
  return (StoryComponent: StoryFn, { globals }: Preview) => {
    const { theme } = globals;

    return (
      <ThemeContextProvider initialTheme={initialTheme ?? theme}>
        <div className={`app ${initialTheme ?? theme}`}>
          <Suspense fallback={<Loader />}>
            <StoryComponent />
          </Suspense>
        </div>
      </ThemeContextProvider>
    );
  };
};
