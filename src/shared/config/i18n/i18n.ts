import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import Backend from 'i18next-http-backend';
import enTransltaions from 'shared/config/locales/en/allTranslations';
import ruTransltaions from 'shared/config/locales/ru/allTranslations';
import uaTransltaions from 'shared/config/locales/uk/allTranslations';

// import { uaTransltaions, ruTransltaions, enTransltaions } from '../locales';

i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    partialBundledLanguages: true,
    fallbackLng: 'en',
    lng: 'en',
    debug: __IS_DEV__,

    // backend: {
    //   loadPath: '/locales/{{lng}}/{{ns}}.json',
    // },
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
  });

i18n
  .addResources('en', 'translation', enTransltaions)
  .addResources('uk', 'translation', uaTransltaions)
  .addResources('ru', 'translation', ruTransltaions);

export default i18n;
