import { render } from '@testing-library/react';
import { StoreProvider } from 'app/providers/StoreProvider';
import { StateSchema } from 'app/providers/StoreProvider/config/StateSchema';
import { DeepPartial } from 'custom-types';
import {
  Component,
  FC,
  ReactNode,
  //  ReactNode,
  Suspense,
} from 'react';
import { I18nextProvider } from 'react-i18next';
import { MemoryRouter } from 'react-router-dom';
import i18n from 'shared/config/i18n/i18n';
import { PageLoader } from 'widgets';

// import i18nForTests from 'shared/config/i18n/i18nForTest';

export interface ComponentRenderOptions {
  route?: string;
  initialState?: DeepPartial<StateSchema>;
}

export function componentRender(
  Component: FC,
  options: ComponentRenderOptions = { route: '/' }
) {
  return render(
    <StoreProvider initialState={options.initialState}>
      <MemoryRouter initialEntries={[options.route]}>
        <I18nextProvider i18n={i18n}>
          <Suspense fallback={<PageLoader />}>
            <Component />
          </Suspense>
        </I18nextProvider>
      </MemoryRouter>
    </StoreProvider>
  );
}
