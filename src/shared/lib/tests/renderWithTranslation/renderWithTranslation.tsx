import { render } from '@testing-library/react';
import {
  FC,
  //  ReactNode,
  Suspense,
} from 'react';

// import i18nForTests from 'shared/config/i18n/i18nForTest';
import { PageLoader } from 'widgets';

export function renderWithTranslation(Component: FC) {
  return render(
    <Suspense fallback={<PageLoader />}>
      <Component />
    </Suspense>
  );
}
