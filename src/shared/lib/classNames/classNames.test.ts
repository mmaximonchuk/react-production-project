import { classNames } from 'shared/lib/classNames/classNames';

describe('classNames', () => {
  test('with only first param', () => {
    const initialClass = 'someClass';
    const expected = 'someClass ';
    expect(classNames(initialClass)).toBe(expected);
  });

  test('with additional params', () => {
    const initialClass = 'someClass';
    const additionalClasses = ['testClass1', 'testClass2'];
    const expected = 'someClass testClass1 testClass2 ';
    expect(classNames(initialClass, {}, additionalClasses)).toBe(expected);
  });

  test('with truly mod param', () => {
    const initialClass = 'someClass';
    const additionalClasses = ['testClass1', 'testClass2'];
    const expected = 'someClass testClass1 testClass2 hovered';

    expect(classNames(initialClass, { hovered: true }, additionalClasses)).toBe(
      expected
    );
  });

  test('with falsy mod param', () => {
    const initialClass = 'someClass';
    const additionalClasses = ['testClass1', 'testClass2'];
    const expected = 'someClass testClass1 testClass2 ';

    expect(
      classNames(initialClass, { hovered: false }, additionalClasses)
    ).toBe(expected);
  });

  test('with undefined mod param', () => {
    const initialClass = 'someClass';
    const additionalClasses = ['testClass1', 'testClass2'];
    const expected = 'someClass testClass1 testClass2 scrollable';

    expect(
      classNames(
        initialClass,
        { hovered: undefined, scrollable: true },
        additionalClasses
      )
    ).toBe(expected);
  });
});
