module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: [
    'plugin:react/recommended',
    'prettier',
    'airbnb',
    'plugin:i18next/recommended',
    'plugin:storybook/recommended',
  ],
  overrides: [
    {
      files: ['**/src/**/*.{test,stories}.{ts,tsx}'],
      rules: {
        'i18next/no-literal-string': 0,
        'max-len': 0,
      },
    },
    {
      env: {
        node: true,
      },
      files: ['.eslintrc.{js,cjs}'],
      parserOptions: {
        sourceType: 'script',
        ecmaFeatures: {
          jsx: true,
        },
      },
    },
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: ['react', 'react-hooks', '@typescript-eslint', 'i18next'],
  rules: {
    'react/react-in-jsx-scope': 0,
    'linebreak-style': [0, 'windows'],
    'react/jsx-indent': [2, 2],
    'react/jsx-filename-extension': [
      1,
      { extensions: ['.tsx', '.jsx', '.js', '.ts'] },
    ],
    'import/no-unresolved': 0,
    'import/prefer-default-export': 0,
    'no-unused-vars': 1,
    indent: [1, 2],
    'react/require-default-props': 0,
    quotes: [1, 'single'],
    'react/jsx-props-no-spreading': 1,
    'react/function-component-definition': 0,
    'no-shadow': 0,
    'import/extensions': 0,
    'import/no-extraneous-dependencies': 0,
    'no-underscore-dangle': 0,
    'no-undef': 0,
    'comma-dangle': 0,
    'max-len': [2, { ignoreComments: true, code: 100 }],
    'i18next/no-literal-string': [
      1,
      { markupOnly: true, ignoreAttribute: ['data-testid', 'to', 'as'] },
    ],
    'object-curly-newline': 0,
    'jsx-quotes': [1, 'prefer-single'],
    'arrow-body-style': 0,
    'import/no-absolute-path': 0,
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'no-param-reassign': 0,
  },
  globals: {
    __IS_DEV__: true,
  },
};
