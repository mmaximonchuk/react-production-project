/* eslint-disable */

class EventEmitter {
  #listeners = {
    // [eventName]: [callback, callback, ...]
  };

  #getCallbacksFor(eventName) {
    return this.#listeners[eventName] ?? [];
  }

  #setCallbacksFor(eventName, listeners) {
    if (listeners.length === 0) {
      delete this.#listeners[eventName];
      return;
    }

    this.#listeners[eventName] = listeners;
  }

  subscribe(eventName, callback) {
    const subs = this.#getCallbacksFor(eventName);

    subs.push(callback);

    this.#setCallbacksFor(eventName, subs);


    return () => this.unsubscribe(eventName, callback);
  }

  unsubscribe(eventName, callback) {
    const subs = this.#getCallbacksFor(eventName);
    const filteredSubs = subs.filter((sub) => sub !== callback);

    this.#setCallbacksFor(eventName, filteredSubs);
  }

  dispatch(eventName, payload) {
    const subs = this.#getCallbacksFor(eventName);

    subs.forEach((subscriber) => {
      subscriber(payload);
    });
  }
}
