import type { Preview } from '@storybook/react';

import { ThemeDecorator } from '../../src/shared/config/storybook/ThemeDecorator';
// import { I18nDecorator } from '../../src/shared/config/storybook/I18nDecorator';
import { RouterDecorator } from '../../src/shared/config/storybook/RouterDecorator';

// import i18n from '../../src/shared/config/i18n/i18n';

import { Themes } from '../../src/app/providers/ThemeProvider/lib/ThemeContext';

export const globalTypes = {
  theme: {
    name: 'Theme',
    description: 'Global theme for components',
    toolbar: {
      icon: 'circlehollow',
      items: [
        { value: Themes.LIGHT, title: 'Light' },
        { value: Themes.DARK, title: 'Dark' },
      ],
      showName: true,
      dynamicTitle: true,
    },
  },
  locale: {
    name: 'Locale',
    description: 'Internationalization locale',
    toolbar: {
      icon: 'globe',
      items: [
        { value: 'en', title: 'English' },
        { value: 'ru', title: 'Російська' },
        { value: 'uk', title: 'Українська' },
      ],
      showName: true,
    },
  },
};

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: '^on[A-Z].*' },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },

  decorators: [
    ThemeDecorator(Themes.LIGHT),
    RouterDecorator,
    // I18nDecorator,
  ],
};

export default preview;
